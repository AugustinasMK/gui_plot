﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace GUI_Plot
{


    public partial class Form1 : Form
    {

        Point mdown = Point.Empty;
        List<DataPoint> selectedPoints = null;

        public Form1()
        {
            InitializeComponent();
            genGraph(100);
        }

        private void genButton_Click(object sender, EventArgs e)
        {
            Console.WriteLine(numText.Text);
            genGraph((int) numText.Value);
        }

        void genGraph(int numOfPoints)
        {

            chart.Series.Clear();
            chart.ChartAreas.Clear();
            ChartArea CA = chart.ChartAreas.Add("CA");
            Series S1 = chart.Series.Add("S1");
            S1.ChartType = SeriesChartType.Point;
            var rand = new Random();

            for (int x = 0; x <= numOfPoints; x++)
            {
                int y = rand.Next() % numOfPoints;
                S1.Points.AddXY(x + 1, y);
            }
        }

        private void chart_MouseDown(object sender, MouseEventArgs e)
        {
            mdown = e.Location;
            selectedPoints = new List<DataPoint>();
        }

        private void chart_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                chart.Refresh();
                using (Graphics g = chart.CreateGraphics())
                    g.DrawRectangle(Pens.Red, GetRectangle(mdown, e.Location));
            }
        }

        private void chart_MouseUp(object sender, MouseEventArgs e)
        {
            Axis ax = chart.ChartAreas[0].AxisX;
            Axis ay = chart.ChartAreas[0].AxisY;
            Rectangle rect = GetRectangle(mdown, e.Location);

            foreach (DataPoint dp in chart.Series[0].Points)
            {
                int x = (int)ax.ValueToPixelPosition(dp.XValue);
                int y = (int)ay.ValueToPixelPosition(dp.YValues[0]);
                if (rect.Contains(new Point(x, y))) selectedPoints.Add(dp);
            }

            // optionally color the found datapoints:
            foreach (DataPoint dp in chart.Series[0].Points)
                dp.Color = selectedPoints.Contains(dp) ? Color.Red : Color.Black;
        }

        static public Rectangle GetRectangle(Point p1, Point p2)
        {
            return new Rectangle(Math.Min(p1.X, p2.X), Math.Min(p1.Y, p2.Y),
                Math.Abs(p1.X - p2.X), Math.Abs(p1.Y - p2.Y));
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tabControl1.SelectedTab.Name == "Selection")
            {
                Console.WriteLine("Jonasdx");
                richTextBox1.Text = "";
                int c = 0;
                selectedPoints.OrderBy(p => p.XValue).ThenBy(p => p.YValues);
                for (int i = 0; i < selectedPoints.Count; i += 4)
                {
                    richTextBox1.Text += $"({selectedPoints[i].XValue},{selectedPoints[i].YValues[0]})\t";
                    c++;
                    if (c == 4)
                    {
                        richTextBox1.Text += '\n';
                        c = 0;
                    }
                }
            }
        }
    }
}
